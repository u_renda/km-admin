<?php
class FeedbackCodeModel extends CI_Model{
	
	var $table = 'KM_FEEDBACK_CODE';
    var $column_order = array(null,'DISPLAY_NAME','DESCRIPTION'); //set column field database for datatable orderable
    var $column_search = array('DISPLAY_NAME','DESCRIPTION'); //set column field database for datatable searchable 
    var $order = array('ID' => 'asc'); // default order 
 
    
    // public function get_allfbcode(){
        // $query = $this->db->get("KM_FEEDBACK_CODE");
        // return $query->result();
    // }
	
	private function _get_datatables_query(){
         
        $this->db->from($this->table);
 
        $i = 0;
     
        foreach ($this->column_search as $item){
            if($_POST['search']['value']){
                 
                if($i===0){
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
	
	function get_allfbcode(){
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
	
	
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all(){
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
	
	public function get_fbcode_by_id($id){
        $this->db->from($this->table);
        $this->db->where('ID',$id);
        $query = $this->db->get();
 
        return $query->row();
    }
	
	public function save($data){
		$query = $this->db->query('SELECT MAX(ID)+1 as ID FROM '.$this->table);
		$row = $query->row();
		$id = $row->ID;
		if($id == NULL) $id = 1; 
		
        $this->db->insert($this->table, $data);
        // return $this->db->insert_id();
		 return $id;
    }
 
    public function update($where, $data){
        $this->db->update($this->table, $data, $where);
        return $this->db->affected_rows();
    }
 
    public function delete_by_id($id){
        $this->db->where('ID', $id);
        $this->db->delete($this->table);
    }
}
