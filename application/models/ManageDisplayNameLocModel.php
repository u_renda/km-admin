<?php
class ManageDisplayNameLocModel extends CI_Model{
	
	var $table = 'KM_EVA_TAG_DISPLAY_NAME_LOC';
	
	
	public function updateDisplayName($displayName, $id)
	{
		$update = "update KM_EVA_TAG_DISPLAY_NAME_LOC set DISPLAY_NAME = '".$displayName."' WHERE ID = ".$id;
		$this->db->query($update);
		
        return $this->db->affected_rows();
	}
	
	public function save_tag($data){
		$query = $this->db->query('SELECT MAX(ID)+1 as ID FROM '.$this->table);
		$row = $query->row();
		$id = $row->ID;
		
		if($id == NULL){
			$data['ID'] = 1;
		}else{
			$data['ID'] = $row->ID;
		}		
		
        $this->db->insert($this->table, $data);
        // return $this->db->insert_id();
		 return $id;
    }
	
}
