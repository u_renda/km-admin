<?php
class ManageTagsetModel extends CI_Model{
	
	var $table = 'KM_EVA_TAG';
	var $table_loc = 'KM_EVA_TAG_LOC';
    var $column_order = array(null,'KM_EVA_TAG.DISPLAY_NAME','KM_EVA_TAG.DESCRIPTION'); //set column field database for datatable orderable
    var $column_search = array('KM_EVA_TAG_DISPLAY_NAME_LOC.DISPLAY_NAME','KM_EVA_TAG_LOC.DESCRIPTION'); //set column field database for datatable searchable 
    var $order = array('KM_EVA_TAG.ID' => 'asc'); // default order 
	
	
	private function _get_datatables_query(){
         
        $this->db->from($this->table);
		$this->db->join('KM_EVA_TAG_LOC','KM_EVA_TAG_LOC.ID = KM_EVA_TAG.ID');
		$this->db->join('KM_EVA_TAG_DISPLAY_NAME_LOC','KM_EVA_TAG_DISPLAY_NAME_LOC.ID = KM_EVA_TAG.ID');
		
 
        $i = 0;
     
        foreach ($this->column_search as $item){
            if($_POST['search']['value']){
                 
                if($i===0){
                    $this->db->group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
	
	
	function get_allfbcode()
	{
        $this->_get_datatables_query();
        
		if($_POST['length'] != -1)
		{
			$this->db->limit($_POST['length'], $_POST['start']);
			$query = $this->db->get();
			
			return $query->result();
		}
    }
	
	function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all(){
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
	
	public function get_tag_by_id($id)
	{
		$this->db->from($this->table);
		$this->db->join('KM_EVA_TAG_LOC','KM_EVA_TAG_LOC.ID = KM_EVA_TAG.ID');
		$this->db->where('KM_EVA_TAG.ID',$id);
		
        $query = $this->db->get(); 
		
        return $query->row();
    }
	
	public function get_parent_tag_ori($id)
	{
		$this->db->from($this->table);
		$this->db->join('KM_EVA_TAG_LOC','KM_EVA_TAG_LOC.ID = KM_EVA_TAG.ID');
		$this->db->where('KM_EVA_TAG.PARENT_TAG',$id);
		$this->db->order_by('KM_EVA_TAG.ID', "asc");
		
        $query = $this->db->get()->result();  
		
        return $query;
    }
	
	public function get_parent_tag_by($id)
	{
		$this->db->from($this->table);
		$this->db->join('KM_EVA_TAG_LOC','KM_EVA_TAG_LOC.ID = KM_EVA_TAG.ID');
		$this->db->join('KM_EVA_TAG_DISPLAY_NAME_LOC','KM_EVA_TAG_DISPLAY_NAME_LOC.ID = KM_EVA_TAG.ID');
		$this->db->where('KM_EVA_TAG.ID',$id);
		$this->db->order_by('KM_EVA_TAG.ID', "asc");
		
        $query = $this->db->get()->result();  
		
        return $query;
    }
	
	public function newTag()
	{
		
		$name = $this->input->post('name');
		$displayName = $this->input->post('displayName');
		$description = $this->input->post('description');
		$addAction = $this->input->post('addAction');
		
		$tag = new ManageTagsetModel;
		$loc = new ManageTagLocModel;
		$dis = new ManageDisplayNameLocModel;
		
		$addTag = $tag->addNewTag($name,$is_deleted);
		$addLoc = $loc->addNewLoc($description);
		$addDis = $dis->addDisplayName($displayName);
		
		die('new tag');
	}
	
	public function updateTag($name, $is_deleted, $id)
	{
		// ($is_deleted == "N") ? $del = "Y" : $del = "N";
		
		$update = "
			update 
				KM_EVA_TAG 
			set 
				NAME       = '".$name."',
				IS_DELETED = '".$is_deleted."'			
			where 
				ID = ".$id; //echo $update;die();
		
		
		$this->db->query($update);
		
        return $this->db->affected_rows();
	}

	public function get_parent_tags($id)
	{
		$tags = "
			SELECT 
				t1.id , t2.parent_tag,  t2.ID as ID_PARENT, t2.SYSTEM_CODE as home, 
				t3.parent_tag as PARENT_TAG_1, t3.ID as ID_CHILD, t3.SYSTEM_CODE as child, 
				t4.parent_tag as PARENT_TAG_CUCU,  t4.ID as ID_CUCU, t4.SYSTEM_CODE as cucu,  
				t5.ID as ID_CICIT, t5.SYSTEM_CODE as cicit
			FROM 
				KM_EVA_TAG t1
			LEFT JOIN 
				KM_EVA_TAG t2 ON t2.parent_tag = t1.id
			LEFT JOIN 
				KM_EVA_TAG t3 ON t3.parent_tag = t2.id
			LEFT JOIN 
				KM_EVA_TAG t4 ON t4.parent_tag = t3.id
			LEFT JOIN 
				KM_EVA_TAG t5 ON t5.parent_tag = t4.id
			LEFT JOIN 
				KM_EVA_TAG_LOC loc ON t1.id = loc.id 
			WHERE 
				t1.id = ".$id."
			ORDER by 
				t1.id asc";
			
		$hasil = $this->db->query($tags)->result();
		$array = array();
		foreach($hasil as $key =>$dt_fib)
		{
			$id  = $dt_fib->ID;
			$tag = $dt_fib->PARENT_TAG;
			$pid = $dt_fib->ID_PARENT; 
			$tag1 = $dt_fib->PARENT_TAG_1;
			if($id == $id)
			{	
				if($pid == $tag1)
				{
					$array[$id][$dt_fib->HOME]= $dt_fib->HOME;													
					$array[$id][]['anaknya']= array($dt_fib->ID_CHILD,$dt_fib->CHILD);
					$array[$id][]['cucu']= array($dt_fib->ID_CUCU,$dt_fib->CUCU);
					$array[$id][]['cicit']= array($dt_fib->ID_CICIT,$dt_fib->CICIT);
				}				
			}			
		}	
		
		return $array;
	}
	
	
	public function save_tag($data){
		$query = $this->db->query('SELECT MAX(ID)+1 as ID FROM '.$this->table);
		$row = $query->row(); 
		$id = $row->ID;  
		if($id == NULL){
			$data['ID'] = 1;
		}else{
			$data['ID'] = $row->ID;
		}
		
        $this->db->insert($this->table, $data);
        // return $this->db->insert_id();
		 return $id;
    }
	
	public function save($data){
		$query = $this->db->query('SELECT MAX(ID)+1 as ID FROM '.$this->table);
		$row = $query->row(); 
		$id = $row->ID;
		if($id == NULL) $id = 1; 
				
        $this->db->insert($this->table, $data);
        // return $this->db->insert_id();
		 return $id;
    }
 
    public function update($id, $desc, $is_delete)
	{
		
		$update_tag = 
			"UPDATE KM_EVA_TAG 
			SET 
				KM_EVA_TAG.IS_DELETED = '".$is_delete."' 
			WHERE KM_EVA_TAG.ID = ".$id; 
		
		
		$update_loc = 
			"UPDATE KM_EVA_TAG_LOC
			SET 
				KM_EVA_TAG_LOC.DESCRIPTION = '".$desc."'
			WHERE KM_EVA_TAG_LOC.ID = ".$id;
			
		$this->db->query($update_tag);
		$this->db->query($update_loc);
		
        return $this->db->affected_rows();
    }
 
    public function delete_by_id($id){
        $this->db->where('ID', $id);
        $this->db->delete($this->table);
    }
	
	function build_menu($menus, $parent = 0, $level = 0)
    {
		$ret = '<ul>';
		foreach($menus as $m)
		{
			if($m->parent_id == $parent)
			{
					$ret .= '<li>'.set_link($m->url, $m->title);
					$ret .= build_menu($menus, $m->id, $level + 1);
					$ret .= '</li>';

			}
		}
		return $ret.'</ul>';
}
	
}
