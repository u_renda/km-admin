<?php
class ManageAgentModel extends CI_Model{
	
	var $table = 'KM_FU_USER';
    var $column_order = array(
		null,'KM_FU_USER.USERNAME','KM_CE_PERSON.FIRST_NAME',
		'KM_CE_PERSON.LAST_NAME'
		); 
    var $column_search = array(
		'KM_FU_USER.USERNAME','KM_CE_PERSON.FIRST_NAME',
		'KM_CE_PERSON.LAST_NAME'
		); 
    var $order = array('KM_CE_PERSON.FIRST_NAME' => 'asc'); // default order 
	
	var $table_profile = 'KM_FD_PROFILE_TYPE';
	var $column_order_profile = array(null,'NAME'); 
	var $column_search_profile = array('NAME'); 
	var $order_profile = array('NAME' => 'asc'); // default order 
 
	var $table_position = 'KM_CE_TEAM';
	var $column_order_position = array(null,'NAME'); 
	var $column_search_position = array('NAME'); 
	var $order_position = array('NAME' => 'asc'); // default order 
	
	private function _get_datatables_query(){
		//add custom filter here
        if($this->input->post('username')){
            $this->db->like('LOWER(KM_FU_USER.USERNAME)', $this->input->post('username'));
        }
        if($this->input->post('fname')){
            $this->db->like('LOWER(KM_CE_PERSON.FIRST_NAME)', $this->input->post('fname'));
        }
        if($this->input->post('lname')){
            $this->db->like('LOWER(KM_CE_PERSON.LAST_NAME)', $this->input->post('lname'));
        }
		
        $this->db->from($this->table);
		$this->db->join('KM_CE_PERSON', 'KM_CE_PERSON.ID = KM_FU_USER.ID');
		$this->db->join('KM_AGENT', 'KM_AGENT.USERNAME = KM_FU_USER.USERNAME');
		$this->db->join('KM_TSEL_AGENT', 'KM_TSEL_AGENT.ID = KM_FU_USER.ID');
		
        $i = 0;
     
        foreach ($this->column_search as $item){
            if($_POST['search']['value']){
                 
                if($i===0){
                    $this->db->group_start();
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else if(isset($this->order)){
            $order = $this->order;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
	
	function get_allagent(){
        $this->_get_datatables_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
		return $query->result();
    }
	
    function count_filtered(){
        $this->_get_datatables_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all(){
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }
	
	public function get_agent_by_id($id){
        $this->db->from($this->table);
		$this->db->join('KM_CE_PERSON', 'KM_CE_PERSON.ID = KM_FU_USER.ID');
		$this->db->join('KM_AGENT', 'KM_AGENT.USERNAME = KM_FU_USER.USERNAME');
		$this->db->join('KM_TSEL_AGENT', 'KM_TSEL_AGENT.ID = KM_FU_USER.ID');
        $this->db->where('KM_FU_USER.ID',$id);
        $query = $this->db->get();
 
        return $query->row();
    }
	
	private function _get_datatables_profile_query(){
		$this->db->from($this->table_profile);
		
        $i = 0;
     
        foreach ($this->column_search_profile as $item){
            if($_POST['search']['value']){
                 
                if($i===0){
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search_profile) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order_profile[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else if(isset($this->order_profile)){
            $order = $this->order_profile;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
	
	function get_allprofile(){
        $this->_get_datatables_profile_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
	
	
    function count_filtered_profile(){
        $this->_get_datatables_profile_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all_profile(){
        $this->db->from($this->table_profile);
        return $this->db->count_all_results();
    }
	
	public function getProfileUser($id){
		$this->db->select('KM_FD_USER_PROFILE_TYPE.USER_ID, KM_FD_USER_PROFILE_TYPE.PROFILE_TYPE_ID');
		$this->db->where('KM_FD_USER_PROFILE_TYPE.USER_ID',$id);
		$this->db->join('KM_FD_PROFILE_TYPE','KM_FD_PROFILE_TYPE.ID = KM_FD_USER_PROFILE_TYPE.PROFILE_TYPE_ID');
		$this->db->from('KM_FD_USER_PROFILE_TYPE');
        $query = $this->db->get();
		// print_r($query->result()); die;
        return $query->result();
	}
	
	private function _get_datatables_position_query($id){
			
		$this->db->from($this->table_position);
		$this->db->join('KM_CE_POSITION','KM_CE_POSITION.TEAM_ID = KM_CE_TEAM.ID');
		$this->db->where('KM_CE_POSITION.AGENT_ID',$id);

        $i = 0;
     
        foreach ($this->column_search_position as $item){
            if($_POST['search']['value']){
                 
                if($i===0){
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search_position) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order_position[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else if(isset($this->order_posistion)){
            $order = $this->order_posistion;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
	
	function get_allposition($id){
        $this->_get_datatables_position_query($id);
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
	
	
    function count_filtered_position($id){
        $this->_get_datatables_position_query($id);
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all_position($id){
        $this->db->from($this->table_position);
        $this->db->where('ID',$id);
        return $this->db->count_all_results();
    }
	
	private function _get_datatables_tag_query(){
			
		$this->db->from($this->table_tag);

        $i = 0;
     
        foreach ($this->column_search_tag as $item){
            if($_POST['search']['value']){
                 
                if($i===0){
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search_tag) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order_tag[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else if(isset($this->order_tag)){
            $order = $this->order_tag;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
	
	function get_alltag(){
        $this->_get_datatables_tag_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
	
	
    function count_filtered_tag(){
        $this->_get_datatables_tag_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all_tag(){
        $this->db->from($this->table_tag);
        return $this->db->count_all_results();
    }
	
	private function _get_datatables_team_query(){
		$this->db->from($this->table_position);
		
        $i = 0;
     
        foreach ($this->column_search_position as $item){
            if($_POST['search']['value']){
                 
                if($i===0){
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search_position) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order_position[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else if(isset($this->order_position)){
            $order = $this->order_position;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
	
	function get_allteam(){
        $this->_get_datatables_team_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
	
    function count_filtered_team(){
        $this->_get_datatables_team_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all_team(){
        $this->db->from($this->table_position);
        return $this->db->count_all_results();
    }
	
	public function getTeamUser($id){
		$this->db->select('KM_CE_POSITION.TEAM_ID');
		$this->db->where('KM_CE_POSITION.AGENT_ID',$id);
		$this->db->join('KM_CE_TEAM','KM_CE_TEAM.ID = KM_CE_POSITION.TEAM_ID');
		$this->db->from('KM_CE_POSITION');
        $query = $this->db->get();
        return $query->result();
	}
	
	public function delete_agent_by_id($id){
		$this->db->where('KM_FU_USER.ID', $id);
		$this->db->delete('KM_FU_USER');
		
		$this->db->where('KM_CE_PERSON.ID', $id);
		$this->db->delete('KM_CE_PERSON');
		
		$this->db->where('KM_AGENT.USERNAME IN (SELECT KM_FU_USER.USERNAME FROM KM_FU_USER WHERE KM_FU_USER.ID = '.$id.')');
		$this->db->delete('KM_AGENT');
    }
	
	public function update($whereid='', $where_id_tsel='', $whereuname='', $km_fu_user='', $km_agent='', $km_ce_person='', $km_tsel_agent=''){
        $this->db->update('KM_AGENT', $km_agent, $whereuname);
        $this->db->update('KM_FU_USER', $km_fu_user, $whereid);
        $this->db->update('KM_CE_PERSON', $km_ce_person, $whereid);
        $this->db->update('KM_TSEL_AGENT', $km_tsel_agent, $where_id_tsel);
        return $this->db->affected_rows();
    }
	
	public function add_agent($whereid='', $where_id_tsel='', $whereuname='', $km_fu_user='', $km_agent='', $km_ce_person='', $km_tsel_agent=''){
        $this->db->insert('KM_AGENT', $km_agent, $whereuname);
        $this->db->insert('KM_FU_USER', $km_fu_user, $whereid);
        $this->db->insert('KM_CE_PERSON', $km_ce_person, $whereid);
        $this->db->insert('KM_TSEL_AGENT', $km_tsel_agent, $where_id_tsel);
		
        $this->db->insert('KM_FD_USER_PROFILE_TYPE', $profile_type_id);
    }
	
	public function update_user_profile($profile_type_id='', $userid='', $profile=''){
		$this->db->from('KM_FD_USER_PROFILE_TYPE');
		$this->db->where('USER_ID', $userid);
		$this->db->where('PROFILE_TYPE_ID', $profile);
		$query = $this->db->count_all_results();
		
		if($query > 0 ){
			$this->db->where('USER_ID', $userid);
			$this->db->delete('KM_FD_USER_PROFILE_TYPE');
			
			$this->db->insert('KM_FD_USER_PROFILE_TYPE', $profile_type_id);
		}else{
			$this->db->insert('KM_FD_USER_PROFILE_TYPE', $profile_type_id);
		}
        // $profile_chk = $this->db->count_all_results();
		
		// $this->db->where($profile_type_id);
		// $this->db->from('KM_FD_USER_PROFILE_TYPE')
        // $this->db->insert('KM_FD_USER_PROFILE_TYPE', $profile_type_id);
    }
	
	// public function update_fu_user($data_fu_user, $userid){
		// $this->db->update($this->table, $data, $where);
        // return $this->db->affected_rows();
	// }
	
	public function update_user_profile_clear($userid){
		$this->db->where('USER_ID', $userid);
		$this->db->delete('KM_FD_USER_PROFILE_TYPE');
	}
	
	public function update_agent_team($data_team, $userid, $team_id){
		$this->db->from('KM_CE_POSITION');
		$this->db->where('AGENT_ID', $userid);
		$this->db->where('TEAM_ID', $team_id);
		$query = $this->db->count_all_results();
		if($query > 0 ){
			$this->db->where('AGENT_ID', $userid);
			$this->db->where('TEAM_ID', $team_id);
			$this->db->delete('KM_CE_POSITION');
			
			$this->db->insert('KM_CE_POSITION', $data_team);
		}else{
			$this->db->insert('KM_CE_POSITION', $data_team);
			
		}
	}
	
	public function delete_by_position_id($id){
        $this->db->where('ID', $id);
        $this->db->delete('KM_CE_POSITION');
    }
}
