<?php
class ManageDesktopModel extends CI_Model{
	
	var $table_profile = 'KM_FD_PROFILE_TYPE';
	var $column_order_profile = array(null,'NAME'); 
	var $column_search_profile = array('ID', 'NAME'); 
	var $order_profile = array('NAME' => 'asc'); // default order 
		
	private function _get_datatables_profile_query(){
		$this->db->from($this->table_profile);
		
        $i = 0;
     
        foreach ($this->column_search_profile as $item){
            if($_POST['search']['value']){
                 
                if($i===0){
                    $this->db->group_start(); 
                    $this->db->like($item, $_POST['search']['value']);
                }else{
                    $this->db->or_like($item, $_POST['search']['value']);
                }
 
                if(count($this->column_search_profile) - 1 == $i) //last loop
                    $this->db->group_end(); //close bracket
            }
            $i++;
        }
         
        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order_profile[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
        }else if(isset($this->order_profile)){
            $order = $this->order_profile;
            $this->db->order_by(key($order), $order[key($order)]);
        }
    }
	
	function get_allprofile(){
        $this->_get_datatables_profile_query();
        if($_POST['length'] != -1)
        $this->db->limit($_POST['length'], $_POST['start']);
        $query = $this->db->get();
        return $query->result();
    }
	
	
    function count_filtered_profile(){
        $this->_get_datatables_profile_query();
        $query = $this->db->get();
        return $query->num_rows();
    }
 
    public function count_all_profile(){
        $this->db->from($this->table_profile);
        return $this->db->count_all_results();
    }
	
	public function getProfileUser($id){
		$this->db->select('KM_FD_USER_PROFILE_TYPE.USER_ID, KM_FD_USER_PROFILE_TYPE.PROFILE_TYPE_ID');
		$this->db->where('KM_FD_USER_PROFILE_TYPE.USER_ID',$id);
		$this->db->join('KM_FD_PROFILE_TYPE','KM_FD_PROFILE_TYPE.ID = KM_FD_USER_PROFILE_TYPE.PROFILE_TYPE_ID');
		$this->db->from('KM_FD_USER_PROFILE_TYPE');
        $query = $this->db->get();
        return $query->result();
	}
	
	public function save_desktop($data){
        $this->db->insert($this->table_profile, $data);
    }
	
	public function get_desktop_by_id($id){
		$this->db->from($this->table_profile);
        $this->db->where('ID',$id);
        $query = $this->db->get();
 
        return $query->row();
	}
	
	public function delete_by_id($id){
		
		$this->db->where('ID', $id);
        $this->db->delete($this->table_profile);
	}
	
}
