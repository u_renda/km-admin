<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>IKnow Telkomsel | Admin</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
            folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css">
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <!-- header -->
            <?php include("includes/header.php"); ?>
            <!-- Left side column. contains the logo and sidebar -->
            <?php include("includes/left-side-menu.php"); ?>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>Manage Agents</h1>
					<p>Use the options below to manage Agents.</p>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Manage Agent</li>
                    </ol>
                </section>
                <!-- Main content -->
                <section class="content">
                    <!-- /.box -->
					<div class="row">
						<div class="col-xs-12">
							<div class="box">
								<div class="box-header with-border">
								  <h3 class="box-title">Search</h3>
								</div>
								<!-- /.box-header -->
								<!-- form start -->
								<form id="form-filter" class="form-horizontal">
								  <div class="box-body">
									<div class="form-group">
									
									  <label for="username" class="col-sm-2 control-label">Username:</label>
									  <div class="col-sm-10" >
										<input type="text" data-column="0" class="form-control input-sm" id="username" name="username" placeholder="Username">
									  </div>
									</div>
									<div class="form-group">
									  <label for="firstName" class="col-sm-2 control-label">First Name:</label>
									  <div class="col-sm-10" >
										<input type="text" data-column="1" class="form-control input-sm" id="fname" name="fname" placeholder="First Name">
									  </div>
									</div>
									<div class="form-group">
									  <label for="lastName" class="col-sm-2 control-label">Last Name:</label>
									  <div class="col-sm-10" >
										<input type="text" data-column="2" class="form-control input-sm" id="lname"  name="lname" placeholder="Last Name">
									  </div>
									</div>
								  </div>
								  <!-- /.box-body -->
								  <div class="box-footer">
							
									<button type="button" class="btn btn-sm btn-primary" id="btn-clear">Clear</button>
									<button type="button" class="btn btn-sm btn-primary" id="btn-filter">Search</button>
								  </div>
								  <!-- /.box-footer -->
								</form>
							</div>
						</div>
					</div>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box">
                                <div class="box-header">
                                    <h3 class="box-title">Result</h3>
									
                                </div>
                                <div class="box-body">
                                    <table id="tbl_feedback" class="table table-bordered table-striped">
                                        <thead>
                                            <tr>
                                                <th>Username</th>
                                                <th>First Name</th>                                         
                                                <th>Last Name</th>                                         
                                                <th>Action</th>                                         
                                            </tr>
                                        </thead>
										
                                        <tbody>
                                           
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.box-body -->
                                <div class="box-footer">
								<a href ="manage_agent/add"><button type="button" class="btn btn-success pull-right" >Add</button></a>
                                </div>
                            </div>
                            <!-- /.box -->
                        </div>
                    </div>
                    <!-- /.row -->
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <!-- footer -->
            <?php include("includes/footer.php"); ?>	
            <!-- Add the sidebar's background. This div must be placed
                immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
        </div>
        <!-- ./wrapper -->
        <!-- jQuery 2.2.3 -->
        <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
        <!-- DataTables -->
        <script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url(); ?>assets/dist/js/app.min.js"></script>
        <script>					
			var table;		
			$(document).ready(function() {
				
				$('#btn-filter').click(function(){ //button filter event click
					if(table){ table.destroy(); }
					
					if($('#username').val() !='' || $('#fname').val() !='' || $('#lname').val() !=''){
						table = $('#tbl_feedback').DataTable({ 

							"processing": true, //Feature control the processing indicator.
							"serverSide": true, //Feature control DataTables' server-side processing mode.
							"order": [], //Initial no order.

							// Load data for the table's content from an Ajax source
							"ajax": {
								"url": "<?php echo site_url('manage_agent/ajax_agent_list')?>",
								"type": "POST",
								"data": function ( data ) {
									data.username = $('#username').val();
									data.fname = $('#fname').val();
									data.lname = $('#lname').val();
								}
							},

							//Set column definition initialisation properties.
							"columnDefs": [
								{ 
									"targets": [ 0 ], //first column / numbering column
									"orderable": false, //set not orderable
								},
							],
						});
					}
				});
				
				$('#btn-clear').click(function(){ //button reset event click
					$('#form-filter')[0].reset();
					table.ajax.reload();  //just reload table
				});

			});
			
			
			function add_fbcode(){
				save_method = 'add';
				$('#form')[0].reset(); // reset form on modals
				$('.form-group').removeClass('has-error'); // clear error class
				$('.help-block').empty(); // clear error string
				$('#modal_form').modal('show'); // show bootstrap modal
				$('.modal-title').text('Add Feedback Code'); // Set Title to Bootstrap modal title
			}
				
			function edit_agent(id){
				save_method = 'update';
				$.ajax({
					url : "<?php echo site_url('manage_agent/ajax_agent_edit/')?>" + id,
					type: "GET",
					dataType: "JSON",
					success: function(data){

						$('[name="id"]').val(data.ID);
						$('[name="username"]').val(data.USERNAME);
						$('[name="fname"]').val(data.FIRST_NAME);
						$('[name="lname"]').val(data.LAST_NAME);

					},
					error: function (jqXHR, textStatus, errorThrown){
						alert('Error get data from ajax');
					}
				});
			}
			
			function reload_table(){
				table.ajax.reload(null,false); //reload datatable ajax 
			}
			
			function save(){
				$('#btnSave').text('saving...'); //change button text
				$('#btnSave').attr('disabled',true); //set button disable 
				var url;

				if(save_method == 'add') {
					url = "<?php echo site_url('feedback_code/ajax_fbcode_add')?>";
				} else {
					url = "<?php echo site_url('feedback_code/ajax_fbcode_update')?>";
				}

				// ajax adding data to database
				$.ajax({
					url : url,
					type: "POST",
					data: $('#form').serialize(),
					dataType: "JSON",
				
					success: function(data){
						if(data.status){
							$('#modal_form').modal('hide');
							reload_table();
						}else{
							for (var i = 0; i < data.inputerror.length; i++){
								$('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
								$('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
							}
						}
						$('#btnSave').text('save'); //change button text
						$('#btnSave').attr('disabled',false); //set button enable 
					},
					error: function (jqXHR, textStatus, errorThrown){
						alert('Error adding / update data');
						$('#btnSave').text('save'); //change button text
						$('#btnSave').attr('disabled',false); //set button enable 
					}
				});
			}
			
			function delete_agent(id){
				if(confirm('Are you sure delete this data?')){
					// ajax delete data to database
					$.ajax({
						url : "<?php echo site_url('manage_agent/ajax_agent_delete')?>/"+id,
						type: "POST",
						dataType: "JSON",
						success: function(data)
						{
							//if success reload ajax table
							$('#modal_form').modal('hide');
							reload_table();
						},
						error: function (jqXHR, textStatus, errorThrown)
						{
							alert('Error deleting data');
						}
					});
			 
				}
			}
			
			/* function save(){
				$('#btnSave').text('saving...'); //change button text
				$('#btnSave').attr('disabled',true); //set button disable 
				var url;

				if(save_method == 'add') {
					url = "<?php echo site_url('manage_agent/ajax_agent_add')?>";
				} else {
					url = "<?php echo site_url('manage_agent/ajax_agent_update')?>";
				}

				// ajax adding data to database
				$.ajax({
					url : url,
					type: "POST",
					data: $('#form').serialize(),
					dataType: "JSON",
				
					success: function(data){
						if(data.status){
							$('#modal_form').modal('hide');
							reload_table();
						}else{
							for (var i = 0; i < data.inputerror.length; i++){
								$('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); 
								$('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); 
							}
						}
						$('#btnSave').text('save'); 
						$('#btnSave').attr('disabled',false);  
					},
					error: function (jqXHR, textStatus, errorThrown){
						alert('Error adding / update data');
						$('#btnSave').text('save'); //change button text
						$('#btnSave').attr('disabled',false); //set button enable 
					}
				});
			} */
        </script>
		<!-- Bootstrap modal -->
		<div class="modal fade" id="modal_form" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h3 class="modal-title">Feedback Code Form</h3>
					</div>
					<div class="modal-body form">
						<form action="#" id="form" class="form-horizontal">
							<input type="hidden" value="" name="id"/> 
							<div class="form-body">
								<div class="form-group">
									<label class="control-label col-md-3">Name:* </label>
									<div class="col-md-9">
										<input name="name" placeholder="Feedback Code Name" class="form-control" type="text">
										<span class="help-block"></span>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Display Name:*</label>
									<div class="col-md-9">
										<input name="displayName" placeholder="Display Name" class="form-control" type="text">
										<span class="help-block"></span>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Description</label>
									<div class="col-md-9">
										<textarea class="textarea" name="desc" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
										<span class="help-block"></span>
									</div>
								</div>
								<div class="form-group">
                                    <label class="control-label col-md-3">Additional Action Required:</label>
                                    <div class="col-sm-9">
                                         <div class="radio">
											<label>
												<input type="radio" name="addAction" id="addActionYes" value="Y">Yes
											</label>
											<label>
												<input type="radio" name="addAction" id="addActionNo" value="N">No
											</label>
										  </div>
                                    </div>
                                </div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
		<!-- End Bootstrap modal -->
    </body>
</html>

