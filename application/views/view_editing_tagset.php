<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>IKnow Telkomsel | Admin</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		
		<!-- EasyTree -->
		<!--<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css"> -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/easytree/css/easyTree.css">

		<!-- <link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css"> 
		<!--<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>-->
		
		
		
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
            folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css">
        <!-- JS Tree -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/jstree/themes/default/style.min.css">
		<!-- Custom -->
		<link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
		
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <!-- header -->
            <?php include("includes/header.php"); ?>
            <!-- Left side column. contains the logo and sidebar -->
            <?php include("includes/left-side-menu.php"); ?>
            <!-- Content Wrapper. Contains page content -->
            
			<div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>Editing Tag</h1>
					<p>
					<!--<p>Use the options below to select the reason you found the content unhelpful, or add the reason if it doesn't appear in the list</p>-->
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Manage Tag</li>
                    </ol>
                </section>
                
				<!-- Main content -->
                <div class="col-md-9">
			  <div class="nav-tabs-custom">
				<ul class="nav nav-tabs">
				  <li class="active"><a href="#information" data-toggle="tab">Information</a></li>
				  <li><a href="#tags" data-toggle="tab">Tags</a></li>
				</ul>
				<div class="tab-content">
					<!-- tab information -->
					<div class="active tab-pane" id="information"> <?php //echo '<pre>'; print_r($tagset_edit);?>
						<form class="form-horizontal" method="POST" action="<?php echo site_url('editing_tagset/tag_update') ?>">
							<div class="form-group">
								<label for="name" class="col-sm-2 control-label">Name</label>
								
								<div class="col-sm-10">
									<input type="text" class="form-control" id="name" name="name" placeholder="Name" value="<?php echo $tagset_edit->NAME; ?>">
								</div>
							</div>
							<div class="form-group">
								<label for="displayName" class="col-sm-2 control-label">Display Name</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" id="displayName" name="displayName" placeholder="Display Name" value="<?php echo $tagset_edit->DISPLAY_NAME; ?>">
								</div>
							</div>
							<div class="form-group">
								<label for="description" class="col-sm-2 control-label">Description</label>

								<div class="col-sm-10">
									<input type="text" class="form-control" id="description" name="description" placeholder="Description" value="<?php echo $tagset_edit->DESCRIPTION; ?>">
								</div>
							</div>
							<div class="form-group">
								<label for="inputDescription" class="col-sm-2 control-label">Is System?</label>
									<div class="col-sm-10">
										<?php if ($tagset_edit->IS_SYSTEM == 'Y'){ ?>
											<label><input type="checkbox" name="is_system" value="<?php echo $tagset_edit->IS_SYSTEM; ?>" <?php echo set_checkbox('is_system', $tagset_edit->IS_SYSTEM, TRUE); ?> /></label>
										<?php } else { ?>
											<label><input type="checkbox" name="is_system" value="<?php echo $tagset_edit->IS_SYSTEM; ?>" <?php echo set_checkbox('is_system', $tagset_edit->IS_SYSTEM, FALSE); ?> /></label>
										<?php } ?>
										
								  </div>
							</div>
							<input type="hidden" name="id_tag" id="id_tag" value="<?php echo $tagset_edit->ID; ?>">
							<div class="form-group">
								<div class="col-sm-offset-2 col-sm-10">
									<button type="button" class="btn btn-danger" onclick="history.go(-1);return false;">Cancel</button>
									<button type="submit" class="btn btn-primary">Submit</button>
								</div>
							</div>
						</form>
					</div>
					
					<div class="tab-pane" id="tags">
						<div class="row mb-lg mt-md">
							<div class="col-md-4 col-sm-8 col-xs-8">
								<button type="button" class="btn btn-success btn-sm" onclick="demo_create();"><i class="glyphicon glyphicon-asterisk"></i> Create</button>
								<button type="button" class="btn btn-warning btn-sm" onclick="demo_rename();"><i class="glyphicon glyphicon-pencil"></i> Rename</button>
								<button type="button" class="btn btn-danger btn-sm" onclick="demo_delete();"><i class="glyphicon glyphicon-remove"></i> Delete</button>
							</div>
						</div>
						<div id="jstree"></div>
					</div>
				  <!-- /.tab-pane -->
				</div>
				<!-- /.tab-content -->
			  </div>
			  <!-- /.nav-tabs-custom -->
			</div>
                <!-- /.content -->
				
				
            </div>
            <!-- /.content-wrapper -->
            <!-- footer -->
            <?php include("includes/footer.php"); ?>	
            <!-- Add the sidebar's background. This div must be placed
                immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
        </div>
        <!-- ./wrapper -->
        <!-- jQuery 2.2.3 -->
        <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
        <!-- DataTables -->
        <script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
        <!-- date-range-picker -->
        <script src="<?php echo base_url(); ?>assets/js/moment.min.js"></script>
        <!-- AdminLTE App -->
        <script src="<?php echo base_url(); ?>assets/dist/js/app.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>
        <!-- JS Tree -->
        <script src="<?php echo base_url(); ?>assets/dist/jstree/jstree.min.js"></script>
		
        
		<script type="text/javascript">
			function demo_create() {
				var ref = $('#jstree').jstree(true),
					sel = ref.get_selected('true'), newNode;
					
				if ( ! sel.length) { return false; }
				
				sel = sel[0];
				newSel = ref.create_node(sel.id, {
					"text": "Tag Baru"
				}, "last", function(response) {
					newNode = response;
					$.get("editing_tagset/tag_create", { data: sel }, function(result){
						//alert("Data: " + result);
						newSel = result;
					});
				});
				
				if(newSel) {
					editSel = ref.edit(newNode, null, function() {
						$.get("editing_tagset/tag_edit", { data: newNode, dataNew: newSel });
					});
				}
				return false;
			};
			function demo_rename() {
				var ref = $('#jstree').jstree(true),
					sel = ref.get_selected('true');
				
				if ( ! sel.length) { return false; }
				
				sel = sel[0];
				
				if (sel.parent !== '#') {
					ref.edit(sel, null, function() {
						$.get("editing_tagset/tag_edit", { data: sel });
					});
				}
				return false;
			};
			function demo_delete() {
				var ref = $('#jstree').jstree(true),
					sel = ref.get_selected('true');
					
				if( ! sel.length) { return false; }
				
				ref.delete_node(sel);
				$.get("editing_tagset/tag_delete", { data: sel });
				return false;
			};

			$(function () {
				var split = window.location.href.split('?id=');
				var id = split[1];
				
				$('#jstree').jstree({
					"core" : {
					  "animation" : 0,
					  "check_callback" : true,
					  "themes" : { "stripes" : true },
					  'data' : {
						'url' : function (node) {
							return node.id === '#' ? 'editing_tagset/tag_tree?id_tag=' + id : '';
						},
						'data' : function (node) {
						  return { 'id' : node.id };
						},
						"dataType" : "json"
					  }
					},
					"types" : {
					  "#" : {
						"max_children" : 1,
						"max_depth" : 4,
						"valid_children" : ["root"]
					  },
					  "root" : {
						"icon" : "/static/3.3.4/assets/images/tree_icon.png",
						"valid_children" : ["default"]
					  },
					  "default" : {
						"valid_children" : ["default","file"]
					  },
					  "file" : {
						"icon" : "glyphicon glyphicon-file",
						"valid_children" : []
					  }
					},
					"checkbox" : {
						"keep_selected_style" : false
					},
					"plugins" : [ "contextmenu" ]
				});
			});
		</script>
		
		
		<!-- Bootstrap modal -->
		<div class="modal fade" id="modal_form" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h3 class="modal-title">Manage Tag Form</h3>
					</div>
					<div class="modal-body form">
						<form action="#" id="form" class="form-horizontal">
							<input type="hidden" value="" name="id"/> 
							<div class="form-body">
								<!--
								<div class="form-group">
									<label class="control-label col-md-3">Name:* </label>
									<div class="col-md-9">
										<input name="name" placeholder="Feedback Code Name" class="form-control" type="text">
										<span class="help-block"></span>
									</div>
								</div>-->
								<div class="form-group">
									<label class="control-label col-md-3">Tag Name:*</label>
									<div class="col-md-9">
										<input name="desc" placeholder="Tag Name" class="form-control" type="text">
										<span class="help-block"></span>
									</div>
								</div>
	
								<div class="form-group">
                                    <label class="control-label col-md-3">Is Deleted</label>
                                    <div class="col-sm-9">
                                         <div class="radio">
											<label>
												<input type="radio" name="addAction" id="addActionYes" value="Y">Yes
											</label>
											<label>
												<input type="radio" name="addAction" id="addActionNo" value="N">No
											</label>
										  </div>
                                    </div>
                                </div>
							</div>
						</form>
					</div>
					<div class="modal-footer">
						<button type="button" id="btnSave" onclick="save()" class="btn btn-primary">Save</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
		<!-- End Bootstrap modal -->
    </body>
</html>

