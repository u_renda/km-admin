<?php //echo $uri; //print_r($profile_user); 
// print_r($data);
 ?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>IKnow Telkomsel | Admin</title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.6 -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/AdminLTE.min.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
            folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/dist/css/skins/_all-skins.min.css">
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <!-- header -->
            <?php include("includes/header.php"); ?>
            <!-- Left side column. contains the logo and sidebar -->
            <?php include("includes/left-side-menu.php"); ?>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>View/Edit Agent</h1>
                    <p>Use the options below to manage Agents.</p>
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                        <li class="active">Manage Agent</li>
                    </ol>
                </section>
                <!-- Main content -->
                <section class="content">
					<form action="#" id="form-manage-agent">
						<div class="box box-default">
							<div class="box-header with-border">
								<h3 class="box-title"></h3>
								<div class="box-tools pull-right">
									<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
									<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
								</div>
							</div>
							<!-- /.box-header -->
							<div class="box-body">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="col-sm-4 control-label">Title:* </label>
											<div class="col-sm-8" style="padding-bottom:5px">
												<select class="form-control input-sm" style="width: 100%;" name="title">
													<?php 
														foreach($opt as $key => $title){ 
															$selected = ($data->TITLE_ID == $key ? 'selected = "selected"':'');
															echo'
															<option '.$selected.' value='.$key.'>'.$title.'</option>';
														} 
													?>
												</select>										
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-4 control-label">First Name:* </label>
											<div class="col-sm-8" style="padding-bottom:5px">
												<input name="fname" placeholder="Feedback Code Name" class="form-control input-sm" type="text" value="<?php echo $data->FIRST_NAME;?>">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-4 control-label">Middle Name: </label>
											<div class="col-sm-8" style="padding-bottom:5px">
												<input name="midname" placeholder="Middle Name" class="form-control input-sm" type="text" value="<?php echo $data->MIDDLE_NAME;?>">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-4 control-label">Last Named:* </label>
											<div class="col-sm-8" style="padding-bottom:5px">
												<input name="lname" placeholder="Feedback Code Name" class="form-control input-sm" type="text" value="<?php echo $data->LAST_NAME;?>">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-4 control-label">Previous Last Name: </label>
											<div class="col-sm-8" style="padding-bottom:5px">
												<input name="prevname" placeholder="Previous Name" class="form-control input-sm" type="text" value="<?php echo $data->PREVIOUS_LAST_NAME;?>">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-4 control-label">Nickname: </label>
											<div class="col-sm-8" style="padding-bottom:5px">
												<input name="nickname" placeholder="Nick Name" class="form-control input-sm" type="text" value="<?php echo $data->NICKNAME;?>">
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-4 control-label">Email Address:* </label>
											<div class="col-sm-8" style="padding-bottom:5px">
												<input name="email" placeholder="Email Address" class="form-control input-sm" type="email" value="<?php echo $data->EMAIL_ADDRESS;?>">
											</div>
										</div>
										<!-- /.form-group -->
									</div>
									<!-- /.col -->
									<div class="col-md-6">
										<div class="form-group">
											<label class="col-sm-4 control-label">Username:* </label>
											<div class="col-sm-6" style="padding-bottom:5px">
												<input name="userid" type="hidden" value="<?php echo $uri; ?>">
												<input name="username" placeholder="Title" class="form-control input-sm" type="text" value="<?php echo $data->USERNAME;?>" disabled>
											</div>
											<input type="checkbox">
										</div>
										<div class="form-group">
											<label class="col-sm-4 control-label">Gender:* </label>
											<div class="col-sm-6" style="padding-bottom:5px">
												<select class="form-control input-sm" style="width: 100%;" name="gender">
													<?php 
														foreach($gender as $key => $dt){ 
															$selected = ($data->GENDER_ID == $key ? 'selected = "selected"':'');
															echo'
															<option '.$selected.' value='.$key.'>'.$dt.'</option>';
														} 
													?>
												</select>
											</div>
											<input type="checkbox">
										</div>
										<div class="form-group">
											<label class="col-sm-4 control-label">Locale:* </label>
											<div class="col-sm-8" style="padding-bottom:5px">
												<select class="form-control input-sm" style="width: 100%;" name="locale">
													<?php 
														$locale = ($data->LOCALE_ID == 1 ? 'en-GB':'');
														echo'<option selected="selected" value="1">'.$locale.'</option>';
													?>
												</select>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-4 control-label">Password:* </label>
											<div class="col-sm-6" style="padding-bottom:5px">
												<input name="password" placeholder="Password" class="form-control input-sm" type="password">
											</div>
											<button>Reset</button>
										</div>
										<div class="form-group">
											<label class="col-sm-4 control-label">Virtual Environment:* </label>
											<div class="col-sm-8" style="padding-bottom:5px">
												<select class="form-control input-sm" style="width: 100%;" name="environment">
													<?php 
														foreach($environment as $key => $dt){ 
															$selected = ($data->VIRTUAL_ENVIRONMENT_ID == $key ? 'selected = "selected"':'');
															echo'
															<option '.$selected.' value='.$key.'>'.$dt.'</option>';
														} 
													?>
												</select>
											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-4 control-label">Homepage: </label>
											<div class="col-xs-4" style="padding-bottom:5px">
												<input name="homepage" placeholder="Homepage" class="form-control input-sm" type="text">
											</div>
											<button>Browse</button>
											<button>Clear</button>
										</div>
										<!-- /.form-group -->
									</div>
									<!-- /.col -->
								</div>
								<!-- /.row -->
							</div>
							<!-- /.box-body -->
							<div class="box-footer"></div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<div class="nav-tabs-custom">
									<ul class="nav nav-tabs">
										<li class="active"><a href="#activity" data-toggle="tab">Profile Types</a></li>
										<li ><a href="#timeline" data-toggle="tab">Positions</a></li>
										<li><a href="#settings" data-toggle="tab">Tags</a></li>
									</ul>
									<div class="tab-content">
										<div class="active tab-pane" id="activity">
											<div class="box">
												<div class="box-header">
													<h3 class="box-title"></h3>
												</div>
												<div class="box-body">
													<table id="tbl_profile" class="table table-bordered table-striped">
														<thead>
															<tr>
																<th>Profile Name</th>
																<th>Select For User?</th>                                         
															</tr>
														</thead>
														<tbody>
														</tbody>
													</table>
												</div>
												<div class="box-footer">
												</div>
												<!-- /.box-body -->
												
											</div>
										</div>
										<!-- /.tab-pane -->
										<div class="tab-pane" id="timeline">
											<div class="box">
												<div class="box-header">
													<h3 class="box-title"></h3>
												</div>
												<div class="box-body">
													<table id="tbl_position" class="table table-bordered table-striped">
														<thead>
															<tr>
																<th>Team</th>
																<th>Action</th>                                         
															</tr>
														</thead>
														<tbody>
														</tbody>
													</table>
												</div>
												<div class="box-footer">
													<button type="button" class="btn btn-success pull-right" onclick="add_team()">Add</button>
												</div>
												<!-- /.box-body -->
												
											</div>
										</div>
										<!-- /.tab-pane -->
										<div class="tab-pane" id="settings">
											<div class="box">
												<div class="box-header">
													<h3 class="box-title"></h3>
													
												</div>
												<div class="box-body">
													<table id="tbl_tag" class="table table-bordered table-striped">
														<thead>
															<tr>
																<th>Selected Tags</th>
																<th>Is Retired</th>                                         
																<th>Action</th>                                         
															</tr>
														</thead>
														<tbody>
														   
														</tbody>
														
													</table>
												</div>
												<div class="box-footer">
													<button type="button" class="btn btn-success pull-right" onclick="add_fbcode()">Add</button>
												</div>
											</div>
										</div>
										<!-- /.tab-pane -->
										
										<div class="box-footer">
											<button type="button" class="btn btn-success pull-left" onclick="add_fbcode()">Cancel</button>
											<button type="button" class="btn btn-success pull-right" onclick="save_manage_agent()">Confirm</button>
										</div>
										
									</div>
									<!-- /.tab-content -->
								</div>
								<!-- /.nav-tabs-custom -->
							</div>
						</div>
						<!-- /.row -->
					</form>
					
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <!-- footer -->
            <?php include("includes/footer.php"); ?>	
            <!-- Add the sidebar's background. This div must be placed
                immediately after the control sidebar -->
            <div class="control-sidebar-bg"></div>
        </div>
        <!-- ./wrapper -->
        <!-- jQuery 2.2.3 -->
        <script src="<?php echo base_url(); ?>assets/plugins/jQuery/jquery-2.2.3.min.js"></script>
        <!-- Bootstrap 3.3.6 -->
        <script src="<?php echo base_url(); ?>assets/bootstrap/js/bootstrap.min.js"></script>
        <!-- DataTables -->
        <script src="<?php echo base_url(); ?>assets/plugins/datatables/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/datatables/dataTables.bootstrap.min.js"></script>
        <!-- date-range-picker -->
        <script src="<?php echo base_url(); ?>assets/js/moment.min.js"></script>							
        <!-- AdminLTE App -->
        <script src="<?php echo base_url(); ?>assets/dist/js/app.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <script src="<?php echo base_url(); ?>assets/dist/js/demo.js"></script>
        <script>
		
			// function test(id) {
				// var z = new Array();
				// var localS = localStorage.getItem('chk'); 
				
				// if ( $("#chk_profile"+id).prop("checked") == true ) {
					
				// } else {
					
				// }
				
				// if (localS) {
					// var arr = $.parseJSON(localS);
					// for (var i in arr){
						// z.push(arr[i]);
					// }
				// }

			// }
		
			var table_profile;

			$(document).ready(function() {
				
				

				//datatables
				table_profile = $('#tbl_profile').DataTable({ 

					"processing": true, //Feature control the processing indicator.
					"serverSide": true, //Feature control DataTables' server-side processing mode.
					"order": [], //Initial no order.

					// Load data for the table's content from an Ajax source
					"ajax": {
						"url": "<?php echo site_url('manage_agent/ajax_profile_list')?>",
						"type": "POST",
						"data": {iduser:<?php echo $uri; ?>}
					},

					//Set column definition initialisation properties.
					"columnDefs": [
						{ 
							"targets": [ 0 ], //first column / numbering column
							"orderable": false, //set not orderable
						},		
					],

				});

			});
			
			// Data table for tab position
			var table_position;

			$(document).ready(function() {

				//datatables
				table_position = $('#tbl_position').DataTable({ 

					"processing": true, //Feature control the processing indicator.
					"serverSide": true, //Feature control DataTables' server-side processing mode.
					"order": [], //Initial no order.

					// Load data for the table's content from an Ajax source
					"ajax": {
						"url": "<?php echo site_url('manage_agent/ajax_position_list')?>",
						"type": "POST",
						"data": {iduser:<?php echo $uri; ?>}
						
					},

					//Set column definition initialisation properties.
					"columnDefs": [
						{ 
							"targets": [ 0 ], //first column / numbering column
							"orderable": false, //set not orderable
						},		
					],

				});

			});
			
			var table_team;

			$(document).ready(function() {

				//datatables
				table_team = $('#tbl_team').DataTable({ 

					"processing": true, //Feature control the processing indicator.
					"serverSide": true, //Feature control DataTables' server-side processing mode.
					"order": [], //Initial no order.

					// Load data for the table's content from an Ajax source
					"ajax": {
						"url": "<?php echo site_url('manage_agent/ajax_team_list')?>",
						"type": "POST",
						"data": {iduser:<?php echo $uri; ?>}
					},

					//Set column definition initialisation properties.
					"columnDefs": [
						{ 
							"targets": [ 0 ], //first column / numbering column
							"orderable": false, //set not orderable
						},		
					],

				});
				
				

			});
			function reload_table_position(){
				table_position.ajax.reload(null,false); //reload datatable ajax 
			}
			
			function reload_table_profile(){
				table_profile.ajax.reload(null,false); //reload datatable ajax 
			}
			
			function add_team(){
				save_method = 'add';
				$('#form-add-team')[0].reset(); // reset form on modals
				$('.form-group').removeClass('has-error'); // clear error class
				$('.help-block').empty(); // clear error string
				$('#modal_form_add_team').modal('show'); // show bootstrap modal
				$('.modal-title').text('Add Team'); // Set Title to Bootstrap modal title
			}
			
			function save_manage_agent(){
				// var coba;
				// alert(coba);
				$.ajax({
					url : "<?php echo site_url('manage_agent/ajax_manage_agent_update')?>",
					type: "POST",
					data: $('#form-manage-agent').serialize(),
					dataType: "JSON",
				
					success: function(data){
						reload_table_profile();
						// $('#btnSaveTeam').text('save'); //change button text
						// $('#btnSaveTeam').attr('disabled',false); //set button enable 
					}
				});
			}
			
			function save_team(){
				$.ajax({
					url : "<?php echo site_url('manage_agent/ajax_add_agent_team')?>",
					type: "POST",
					data: $('#form-add-team').serialize(),
					dataType: "JSON",
				
					success: function(data){
						if(data.status){
							$('#modal_form_add_team').modal('hide');
							reload_table_position();
						}else{
							for (var i = 0; i < data.inputerror.length; i++){
								$('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
								$('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
							}
						}
						$('#btnSaveTeam').text('save'); //change button text
						$('#btnSaveTeam').attr('disabled',false); //set button enable 
					},
					error: function (jqXHR, textStatus, errorThrown){
						alert('Error adding / update data');
						$('#btnSaveTeam').text('save'); //change button text
						$('#btnSaveTeam').attr('disabled',false); //set button enable 
					}
					
				});
			}
			
			
			function delete_position(id){
				if(confirm('Are you sure delete this data?')){
					// ajax delete data to database
					$.ajax({
						url : "<?php echo site_url('manage_agent/ajax_position_delete')?>/"+id,
						type: "POST",
						dataType: "JSON",
						success: function(data)
						{
							//if success reload ajax table
							$('#modal_form').modal('hide');
							reload_table_position();
						},
						error: function (jqXHR, textStatus, errorThrown)
						{
							alert('Error deleting data');
						}
					});
			 
				}
			}
        </script>
		<!-- Bootstrap modal -->
		<div class="modal fade" id="modal_form_add_team" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h3 class="modal-title">Feedback Code Form</h3>
					</div>
					<div class="modal-body form">
						<form action="#" id="form-add-team" class="form-horizontal">
							<div class="box">
								<div class="box-header">
									<h3 class="box-title"></h3>
								</div>
								<div class="box-body">
									<table id="tbl_team" class="table table-bordered table-striped">
									<input name="userid" type="hidden" value="<?php echo $uri; ?>">
										<thead>
											<tr>
												<th>Team</th>
												<th>Action</th>                                         
											</tr>
										</thead>
										<tbody>
										</tbody>
									</table>
								</div>
							</div>						

						</form>
					</div>
					<div class="modal-footer">
						<button type="button" id="btnSaveTeam" onclick="save_team()" class="btn btn-primary">Save</button>
						<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
		<!-- End Bootstrap modal -->
    </body>
</html>

