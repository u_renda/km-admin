<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_tagset extends CI_Controller {
	
	// public function __construct()
	function __construct() 
	{
		parent::__construct();
		
		$this->load->model('ManageTagsetModel');	
		$this->load->model('ManageTagLocModel');	
		$this->load->model('ManageDisplayNameLocModel');

	}

	function index()
	{		
		$this->load->view('view_manage_tagset');
	}	
	
	function ajax_feedback_regions()
	{
		$output = array();
		
		$this->db->select('KM_EVA_TAG.ID AS ID, TAGSET_ID, NAME, IS_SYSTEM, DISPLAY_NAME');
		$this->db->from('KM_EVA_TAG');
		$this->db->join('KM_EVA_TAG_DISPLAY_NAME_LOC', 'KM_EVA_TAG.ID = KM_EVA_TAG_DISPLAY_NAME_LOC.ID', 'LEFT');
		$this->db->where('DEPTH_VAL = 0');
		$this->db->order_by('TAGSET_ID', 'ASC');
		$this->db->limit($this->input->post('length'), $this->input->post('start'));
		$query = $this->db->get();
		
		$this->db->select('KM_EVA_TAG.ID AS ID');
		$this->db->from('KM_EVA_TAG');
		$this->db->join('KM_EVA_TAG_DISPLAY_NAME_LOC', 'KM_EVA_TAG.ID = KM_EVA_TAG_DISPLAY_NAME_LOC.ID', 'LEFT');
		$this->db->where('DEPTH_VAL = 0');
		$total = $this->db->count_all_results();
		
		if ($query->num_rows() > 0)
		{
			$data = array();
			
			foreach ($query->result() as $row)
			{
				$temp = array();
				$temp[] = $row->NAME;
				$temp[] = $row->DISPLAY_NAME;
				$temp[] = $row->IS_SYSTEM;
				$temp[] = '<a class="btn btn-sm btn-primary" href="'.base_url().'editing_tagset?id='.$row->ID.'" title="Edit"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
							<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_fbcode('."'".$row->ID."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
				
				$data[] = $temp;
			}
			
			$output = array(
				"draw" => $this->input->post('draw'),
				"recordsTotal" => $total,
				"recordsFiltered" => $total,
				"data" => $data,
			);
		}
		
		echo json_encode($output);
		
		
//		$reg = new ManageTagsetModel;
//		$regions = $reg->get_allfbcode(); 
//		//print_r($regions);
//		$data = array();
//        $no = $_POST['start'];
//        foreach ($regions as $feedback) {
//            $no++;
//            $row = array();
//            $row[] = $feedback->DISPLAY_NAME;
//            $row[] = $feedback->DESCRIPTION;
//            $row[] = $feedback->IS_DELETED;
//			$row[] = '<a class="btn btn-sm btn-primary" href="'.base_url().'editing_tagset?id='.$feedback->ID.'" title="Edit"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
//                  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_fbcode('."'".$feedback->ID."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
//  
//            $data[] = $row;
//        }
// 
//        $output = array(
//			"draw" => $_POST['draw'],
//			"recordsTotal" => $reg->count_all(),
//			"recordsFiltered" => $reg->count_filtered(),
//			"data" => $data,
//        );
//        //output to json format
//        echo json_encode($output);
	}
	
	public function newTag()
	{
		$name = $this->input->post('name');
		$displayName = $this->input->post('displayName');
		$description = $this->input->post('description');
		$addAction = $this->input->post('addAction');
		
		echo $name.'/'.$displayName.'/'.$description.'/'.$addAction; die('save');
	}
	
	public function ajax_tag_edit($id)
	{
		$fbcode = new ManageTagsetModel;
        $data = $fbcode->get_tag_by_id($id);
        echo json_encode($data);
    }
	
	public function ajax_tag_add()
	{
		
		$tag = new ManageTagsetModel;
		$loc = new ManageTagLocModel;
		$dis = new ManageDisplayNameLocModel;
		
        $this->_validate();
        $data = array(
			'NAME' => $this->input->post('name'),
			'IS_DELETED' => $this->input->post('addAction'),
		);
        $insert = $tag->save_tag($data);
		
		$dataLoc = array(
			'DESCRIPTION' => $this->input->post('description')
		);
        $insert = $loc->save_tag($dataLoc);
		
		$dataDisplayName = array(
			'DISPLAY_NAME' => $this->input->post('displayName')
		);
        $insert = $dis->save_tag($dataDisplayName);
		
        echo json_encode(array("status" => TRUE));
    }
	
	public function ajax_fbcode_add()
	{
		
		$fbcode = new FeedbackCodeModel;
        $this->_validate();
        $data = array(
			'NAME' => $this->input->post('name'),
			'DISPLAY_NAME' => $this->input->post('displayName'),
			'DESCRIPTION' => $this->input->post('desc'),
			'ADDITIONAL_ACTION_NEEDED' => $this->input->post('addAction'),
		);
        $insert = $fbcode->save($data);
        echo json_encode(array("status" => TRUE));
    }
 
    public function ajax_tag_update(){
		$tag = new ManageTagsetModel;
        $this->_validate();
        $data = array(
			// 'DISPLAY_NAME' => $this->input->post('displayName'),
			'DESCRIPTION' => $this->input->post('desc'),
			'IS_DELETED' => $this->input->post('addAction'),
		);
		
        // $tag->update(array('ID' => $this->input->post('id')), $data);
        $tag->update($this->input->post('id'), $this->input->post('desc'),$this->input->post('addAction'));
        echo json_encode(array("status" => TRUE));  
    }
 
    public function ajax_fbcode_delete($id){
		$fbcode = new FeedbackCodeModel;
        $fbcode->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }
 
 
    private function _validate(){
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;
 
        if($this->input->post('name') == '')
        {
            $data['inputerror'][] = 'name';
            $data['error_string'][] = 'Name Tag is required';
            $data['status'] = FALSE;
        }
		
		if($this->input->post('displayName') == '')
        {
            $data['inputerror'][] = 'displayName';
            $data['error_string'][] = 'Display Name Tag is required';
            $data['status'] = FALSE;
        }
		
		if($this->input->post('description') == '')
        {
            $data['inputerror'][] = 'description';
            $data['error_string'][] = 'Description is required';
            $data['status'] = FALSE;
        }
 
        if($this->input->post('addAction') == '')
        {
            $data['inputerror'][] = 'addAction';
            $data['error_string'][] = 'Is Deleted is required';
            $data['status'] = FALSE;
        }
 
        if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }
	
	
}
