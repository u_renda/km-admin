<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_desktop_profile extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('ManageDesktopModel');	
	}

	public function index(){
		$this->load->view('view_desktop_profile');
	}
	
	public function ajax_profile_list(){
		$id = $this->input->post('iduser');
		$profile       = new ManageDesktopModel;
		$list = $profile->get_allprofile();
        $data = array();

        $no = $_POST['start'];
        foreach ($list as $dt) {
            $no++;
            $row    = array();
            $row[]  = $dt->NAME;	
			$row[] = '<a class="btn btn-sm btn-primary" href="manage_desktop_profile/edit/'.$dt->ID.'" title="Edit" onclick="edit_desktop('."'".$dt->ID."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
			 <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_desktop('."'".$dt->ID."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
			$data[] = $row;
			
        }

        $output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $profile->count_all_profile(),
			"recordsFiltered" => $profile->count_filtered_profile(),
			"data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}
	
	public function edit($id){
		$data['uri'] =  $this->uri->segment(3);
		$agent = new ManageDesktopModel;
		
		$desktop_type = array(
			'1' => 'Testing',
			'2' => 'UAT',
			'3' => 'Training',
			'4' => 'Production',
		);
		
		$data['desktop_type'] = $desktop_type;
        // $data['data']   = $agent->get_agent_by_id($id);
		$this->load->view('view_desktop_profile_edit', $data);
	}
	
	public function ajax_desktop_add(){
		$desktop_profile = new ManageDesktopModel;
        $this->_validate();
        $data = array(
			'NAME' => $this->input->post('name'),
			'DESKTOP_PROCESS' => $this->input->post('desktopProcess'),
			'IS_DELETED' => $this->input->post('isDeleted')
		);
        $insert = $desktop_profile->save_desktop($data);
        echo json_encode(array("status" => TRUE));
	}
	
	public function ajax_desktop_edit($id){
		$desktop = new ManageDesktopModel;
        $data   = $desktop->get_desktop_by_id($id);
        echo json_encode($data);
    }
	
	public function ajax_desktop_delete($id){
		$desktop = new ManageDesktopModel;
       $data =  $desktop->delete_by_id($id);

        echo json_encode(array("status" => TRUE));
    }
	
	    private function _validate(){
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;
 
        if($this->input->post('name') == ''){
            $data['inputerror'][] = 'name';
            $data['error_string'][] = 'Profile Types Name is required';
            $data['status'] = FALSE;
        }
 
        if($this->input->post('desktopProcess') == ''){
            $data['inputerror'][] = 'desktopProcess';
            $data['error_string'][] = 'Desktop Process is required';
            $data['status'] = FALSE;
        }
 
        if($data['status'] === FALSE){
            echo json_encode($data);
            exit();
        }
    }

}
