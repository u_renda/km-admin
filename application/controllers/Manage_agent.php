<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manage_agent extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model('ManageAgentModel');	
	}

	public function index(){
		$this->load->view('view_manage_agent');
	}	
	
	public function ajax_agent_list(){
		$agent = new ManageAgentModel;
		$list  = $agent->get_allagent();
        $data  = array();
        $no    = $_POST['start'];
        foreach ($list as $feedback) {
            $no++;
            $row = array();
            $row[] = $feedback->USERNAME;
            $row[] = $feedback->FIRST_NAME;
            $row[] = $feedback->LAST_NAME;
			$row[] = '<a class="btn btn-sm btn-primary" href="manage_agent/edit/'.$feedback->ID.'" title="Edit" onclick="edit_agent('."'".$feedback->ID."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
                  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_agent('."'".$feedback->ID."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
  
            $data[] = $row;
        }
 
        $output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $agent->count_all(),
			"recordsFiltered" => $agent->count_filtered(),
			"data" => $data,
        );
        //output to json format
        echo json_encode($output); die;
	}
	
	public function add(){
		$agent = new ManageAgentModel;

		$title = array(
			'' => 'Please Select',
			'1' => 'Mr',
			'2' => 'Mrs',
			'3' => 'Ms',
			'4' => 'Miss',
			'5' => 'Dr',
			'6' => 'Other'
		);
		
		$gender = array(
			'' => 'Please Select',
			'1' => 'Male',
			'2' => 'Female'
		);
		
		$environment = array(
			'1' => 'Testing',
			'2' => 'UAT',
			'3' => 'Training',
			'4' => 'Production',
		);
		$data['opt'] = $title;
		$data['gender'] = $gender;
		$data['environment'] = $environment;
        // $data['data']   = $agent->get_agent_by_id($id);
		$this->load->view('view_agent_add', $data);
	}
		
	public function edit($id){
		$data['uri'] =  $this->uri->segment(3);
		$agent = new ManageAgentModel;

		$title = array(
			'' => 'Please Select',
			'1' => 'Mr',
			'2' => 'Mrs',
			'3' => 'Ms',
			'4' => 'Miss',
			'5' => 'Dr',
			'6' => 'Other'
		);
		
		$gender = array(
			'' => 'Please Select',
			'1' => 'Male',
			'2' => 'Female'
		);
		
		$environment = array(
			'1' => 'Testing',
			'2' => 'UAT',
			'3' => 'Training',
			'4' => 'Production',
		);
		// $data['profile_user'] = $agent->getProfileUser();
		$data['opt'] = $title;
		$data['gender'] = $gender;
		$data['environment'] = $environment;
        $data['data']   = $agent->get_agent_by_id($id);
        // echo json_encode($data);
		$this->load->view('view_agent_edit', $data);
	}
	
	public function ajax_agent_edit($id){
		$agent = new ManageAgentModel;
        $data   = $agent->get_agent_by_id($id);
        echo json_encode($data);

    }
	
	public function ajax_profile_list(){
		$id = $this->input->post('iduser');
		$profile       = new ManageAgentModel;
		$profile_user = $profile->getProfileUser($id);
		$list = $profile->get_allprofile();
        $data = array();

        $no = $_POST['start'];
        foreach ($list as $dt) {
            $no++;
            $row    = array();
            $row[]  = $dt->NAME;
			if(!empty($profile_user)){
				foreach($profile_user as $user){
					$checked = $user->PROFILE_TYPE_ID == $dt->ID ? 'checked':'';
					if($checked != '')
					break;
				}
				$row[]  = '<input align="center" '.$checked.' name="chk_profile[]" type="checkbox" id="chk_profile'.$dt->ID.'" value="'.$dt->ID.'" >';
				
			}else{
				$row[]  = '<input align="center" name="chk_profile[]" type="checkbox" id="chk_profile'.$dt->ID.'" value="'.$dt->ID.'" >';
				
			}
			
			$data[] = $row;
			
        }

        $output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $profile->count_all_profile(),
			"recordsFiltered" => $profile->count_filtered_profile(),
			"data" => $data,
        );
        echo json_encode($output);
	}
	
	public function ajax_profile_list_master(){
		$profile       = new ManageAgentModel;
		$list = $profile->get_allprofile();
		// print_r($list); die;
        $data = array();

        $no = $_POST['start'];
        foreach ($list as $dt) {
            $no++;
            $row    = array();
            $row[]  = $dt->NAME;
			$row[]  = '<input align="center"  name="chk_profile[]" type="checkbox" id="chk_profile'.$dt->ID.'" value="'.$dt->ID.'" >';
			$data[] = $row;
        }

        $output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $profile->count_all_profile(),
			"recordsFiltered" => $profile->count_filtered_profile(),
			"data" => $data,
        );
        echo json_encode($output);
	}
	
	public function ajax_position_list(){
		$id = $this->input->post('iduser');
		$position       = new ManageAgentModel;
		$list = $position->get_allposition($id);
		// print_r($list);
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $dt) {
            $no++;
            $row = array();
            $row[] = $dt->NAME;
			$row[] = '<a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_position('."'".$dt->ID."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
  
            $data[] = $row;
        }
 
        $output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $position->count_all_position($id),
			"recordsFiltered" => $position->count_filtered_position($id),
			"data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}
	
	public function ajax_team_list(){
		$id = $this->input->post('iduser');
		$profile       = new ManageAgentModel;
		$profile_user = $profile->getTeamUser($id);
		$list = $profile->get_allteam();
        $data = array();

        $no = $_POST['start'];
        foreach ($list as $dt) {
            $no++;
            $row    = array();
            $row[]  = $dt->NAME;
			
			if(!empty($profile_user)){
				foreach($profile_user as $user){
					$checked = $user->TEAM_ID == $dt->ID ? 'checked':'';
					if($checked != '')
					break;
				}
				$row[]  = '<input align="center" '.$checked.' name="chk_team[]" type="checkbox" id="chk_team'.$dt->ID.'" value="'.$dt->ID.'" >';
				
			}else{
				$row[]  = '<input align="center" name="chk_team[]" type="checkbox" id="chk_team'.$dt->ID.'" value="'.$dt->ID.'" >';
				
			}
			
			$data[] = $row;
        }
 
        $output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $profile->count_all_team(),
			"recordsFiltered" => $profile->count_filtered_team(),
			"data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}
	
	public function ajax_agent_delete($id){
		$agent = new ManageAgentModel;
        $agent->delete_agent_by_id($id);
        echo json_encode(array("status" => TRUE));
    }
	
	public function ajax_manage_agent_update(){
		$userid = $this->input->post('userid');
		$username = $this->input->post('username');
		$agent = new ManageAgentModel;
        $data_fu_user = array(
			// 'USERNAME' => $username,
			'LOCALE_ID' => $this->input->post('locale'),
			'VIRTUAL_ENVIRONMENT_ID' => $this->input->post('environment')
		);  
		
		$data_km_agent = array(
			// 'USERNAME' => $username,
			'PASSWORD' => $this->input->post('password')
			
		); 
		
		$data_km_ce_person = array(
			'ID' => $userid,
			'FIRST_NAME' => $this->input->post('fname'),
			'FIRST_NAME_UPPER' => strtoupper($this->input->post('fname')),
			'MIDDLE_NAME' => $this->input->post('midname'),
			'LAST_NAME' => $this->input->post('lname'),
			'LAST_NAME_UPPER' => strtoupper($this->input->post('lname')),
			'PREVIOUS_LAST_NAME' => $this->input->post('prevname'),
			'GENDER_ID' => $this->input->post('gender'),
			'TITLE_ID' => $this->input->post('title'),
			'NICKNAME' => $this->input->post('nickname')
		);
		$data_tsel_agent = array(
			'USER_ID' => $userid,
			'EMAIL_ADDRESS' => $this->input->post('email')
		); 
		if($this->input->post('chk_profile') != ''){
			foreach($this->input->post('chk_profile') as $profile){ 
				$data_profile_user = array(
					'USER_ID' => $userid,
					'PROFILE_TYPE_ID' => $profile
				); 
				
				$agent->update_user_profile($data_profile_user, $userid, $profile);
			} 
		}else{
			$agent->update_user_profile_clear($userid);
		}
		
		$agent->update(array('ID' => $userid),array('USER_ID' => $userid), array('USERNAME' => $username), $data_fu_user, $data_km_agent, $data_km_ce_person,$data_tsel_agent);
        echo json_encode(array("status" => TRUE)); die;
	}
	
	public function ajax_add_agent_team(){
		$userid     = $this->input->post('userid');
		$agent_team = new ManageAgentModel;
		if($this->input->post('chk_team') != ''){
			foreach($this->input->post('chk_team') as $team_id){ 
				$data_team = array(
					'AGENT_ID' => $userid,
					'TEAM_ID' => $team_id
				); 
				
				$agent_team->update_agent_team($data_team, $userid, $team_id);
			} 
		}
		 echo json_encode(array("status" => TRUE)); die;
	}
	
	public function ajax_add_agent(){
        $data_fu_user = array(
			'USERNAME' => $username,
			'LOCALE_ID' => $this->input->post('locale'),
			'VIRTUAL_ENVIRONMENT_ID' => $this->input->post('environment')
		);  
		
		$data_km_agent = array(
			'USERNAME' => $username,
			'PASSWORD' => $this->input->post('password')
		); 
		
		$data_km_ce_person = array(
			'ID' => $userid,
			'FIRST_NAME' => $this->input->post('fname'),
			'FIRST_NAME_UPPER' => strtoupper($this->input->post('fname')),
			'MIDDLE_NAME' => $this->input->post('midname'),
			'LAST_NAME' => $this->input->post('lname'),
			'LAST_NAME_UPPER' => strtoupper($this->input->post('lname')),
			'PREVIOUS_LAST_NAME' => $this->input->post('prevname'),
			'GENDER_ID' => $this->input->post('gender'),
			'TITLE_ID' => $this->input->post('title'),
			'NICKNAME' => $this->input->post('nickname')
		);
		$data_tsel_agent = array(
			'USER_ID' => $userid,
			'EMAIL_ADDRESS' => $this->input->post('email')
		); 
		
		$agent = new ManageAgentModel;
		$agent->add_agent(array('ID' => $userid),array('USER_ID' => $userid), array('USERNAME' => $username), $data_fu_user, $data_km_agent, $data_km_ce_person,$data_tsel_agent);
        echo json_encode(array("status" => TRUE)); die;
		
		if($this->input->post('chk_profile') != ''){
			foreach($this->input->post('chk_profile') as $profile){ 
				$data_profile_user = array(
					'USER_ID' => $userid,
					'PROFILE_TYPE_ID' => $profile
				); 
				
				$agent->update_user_profile($data_profile_user, $userid, $profile);
			} 
		}else{
			$agent->update_user_profile_clear($userid);
		}
		 echo json_encode(array("status" => TRUE)); die;
	}
	
	public function ajax_position_delete($id){
		$fbcode = new ManageAgentModel;
        $fbcode->delete_by_position_id($id);
        echo json_encode(array("status" => TRUE));
    }
	
}
