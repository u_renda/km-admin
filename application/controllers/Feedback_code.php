<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feedback_code extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model('FeedbackCodeModel');
		
	}

	public function index(){
		$this->load->view('view_feedback_code');
	}
	
	public function ajax_feedback_list(){
		$fbcode       = new FeedbackCodeModel;
		$list = $fbcode->get_allfbcode();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $feedback) {
            $no++;
            $row = array();
            $row[] = $feedback->DISPLAY_NAME;
            $row[] = $feedback->DESCRIPTION;
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_fbcode('."'".$feedback->ID."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
                  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_fbcode('."'".$feedback->ID."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
  
            $data[] = $row;
        }
 
        $output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $fbcode->count_all(),
			"recordsFiltered" => $fbcode->count_filtered(),
			"data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}
	
	public function ajax_fbcode_edit($id){
		$fbcode = new FeedbackCodeModel;
        $data   = $fbcode->get_fbcode_by_id($id);
        echo json_encode($data);
    }
	
	public function ajax_fbcode_add(){
		$fbcode = new FeedbackCodeModel;
        $this->_validate();
        $data = array(
			'NAME' => $this->input->post('name'),
			'DISPLAY_NAME' => $this->input->post('displayName'),
			'DESCRIPTION' => $this->input->post('desc'),
			'ADDITIONAL_ACTION_NEEDED' => $this->input->post('addAction'),
		);
        $insert = $fbcode->save($data);
        echo json_encode(array("status" => TRUE));
    }
 
    public function ajax_fbcode_update(){
		$fbcode = new FeedbackCodeModel;
        $this->_validate();
        $data = array(
			'NAME' => $this->input->post('name'),
			'DISPLAY_NAME' => $this->input->post('displayName'),
			'DESCRIPTION' => $this->input->post('desc'),
			'ADDITIONAL_ACTION_NEEDED' => $this->input->post('addAction'),
		);
        $fbcode->update(array('ID' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE)); die;
    }
 
    public function ajax_fbcode_delete($id){
		$fbcode = new FeedbackCodeModel;
        $fbcode->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }
 
 
    private function _validate(){
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;
 
        if($this->input->post('name') == '')
        {
            $data['inputerror'][] = 'name';
            $data['error_string'][] = 'Name is required';
            $data['status'] = FALSE;
        }
 
        if($this->input->post('displayName') == '')
        {
            $data['inputerror'][] = 'displayName';
            $data['error_string'][] = 'Display name is required';
            $data['status'] = FALSE;
        }
 
        if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }
	
}
