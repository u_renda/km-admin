<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Editing_tagset extends CI_Controller {
	
	// public function __construct()
	function __construct() 
	{
		parent::__construct();
		
		$this->load->model('ManageTagsetModel');	
		$this->load->model('ManageTagLocModel');	
		$this->load->model('ManageDisplayNameLocModel');	

	}

	function index()
	{
		$data = array();
		$data['id'] = $this->input->get('id');
		
		$output = array();
		$this->db->select('KM_EVA_TAG.ID AS ID, TAGSET_ID, NAME, IS_SYSTEM, DISPLAY_NAME, DESCRIPTION');
		$this->db->from('KM_EVA_TAG');
		$this->db->join('KM_EVA_TAG_DISPLAY_NAME_LOC', 'KM_EVA_TAG.ID = KM_EVA_TAG_DISPLAY_NAME_LOC.ID', 'LEFT');
		$this->db->join('KM_EVA_TAG_LOC', 'KM_EVA_TAG.ID = KM_EVA_TAG_LOC.ID', 'LEFT');
		$this->db->where('KM_EVA_TAG.ID = ', $data['id']);
		$query = $this->db->get();
		
		if ($query->num_rows() > 0)
		{
			$data['tagset_edit'] = $query->row();
			
			$this->load->view('view_editing_tagset', $data);
		}
		else
		{
			echo "Data not found";
		}
		
		
		
		//$id = $_GET['id'];		
		//
		//$tag = new ManageTagsetModel;		
		//$data['tagset'] = $tag->get_parent_tags($id);
		//$data['tagset_edit'] = $tag->get_parent_tag_by($id);
		
		// echo '<pre>';
		// print_r($data['tagset_edit']);
		
		//$this->load->view('view_editing_tagset', $data);
		
	}	
	
	function tag_update()
	{
		$id = $this->input->post('id_tag'); 
		
		$data = array();
		$data2 = array();
		$data3 = array();
		
		$data['NAME'] = $this->input->post('name');
		
		if ($this->input->post('is_system') == TRUE)
		{
			$data['IS_SYSTEM'] = 'Y';
		}
		else
		{
			$data['IS_SYSTEM'] = 'N';
		}
		
		$this->db->where('ID', $id);
		$this->db->update('KM_EVA_TAG', $data);
		
		$data2['DISPLAY_NAME'] = $this->input->post('displayName');
		
		$this->db->where('ID', $id);
		$this->db->update('KM_EVA_TAG_DISPLAY_NAME_LOC', $data2);
		
		$data3['DESCRIPTION'] = $this->input->post('description');
		
		$this->db->where('ID', $id);
		$this->db->update('KM_EVA_TAG_LOC', $data3);
		
		redirect ('manage_tagset/index');
	}
	
	function tag_tree()
	{
		$data = array();
		$id = $this->input->get('id_tag');
		
		$this->db->select('KM_EVA_TAG.ID AS ID, TAGSET_ID, NAME, PARENT_TAG, DISPLAY_NAME');
		$this->db->from('KM_EVA_TAG');
		$this->db->join('KM_EVA_TAG_DISPLAY_NAME_LOC', 'KM_EVA_TAG.ID = KM_EVA_TAG_DISPLAY_NAME_LOC.ID', 'LEFT');
		$this->db->where('TAGSET_ID = ', $id);
		$this->db->order_by('ID', 'ASC');
		$this->db->order_by('PREVIOUS_TAG', 'ASC');
		$query = $this->db->get();
		
		if ($query->num_rows() > 0)
		{
			foreach ($query->result() as $row)
			{
				$temp = array();
				$temp['id'] = $row->ID;
				$temp['text'] = $row->DISPLAY_NAME;
				
				if ($row->PARENT_TAG == NULL)
				{
					$temp['parent'] = '#';
					$temp['state'] = array(
						'opened' => true,
						'selected' => true
					);
				}
				else
				{
					$temp['parent'] = $row->PARENT_TAG;
				}
				
				$data[] = $temp;
			}
			
			echo json_encode($data);
			exit;
		}
	}
	
	function tag_create()
	{
		$get_data = $this->input->get('data');
		$tagset_id = $get_data['id'];
		$parent_tag = $get_data['id'];
		
		if(count($get_data['parents']) > 1)
		{
			$tagset_id = $get_data['parents'][count($get_data['parents']) - 2];
			$parent_tag = $get_data['id'];
		}
		
		// dapetin data yang akan jadi parent-nya
		$this->db->select('ID, DEPTH_VAL');
		$this->db->from('KM_EVA_TAG');
		$this->db->where('ID = ', $get_data['id']);
		$query = $this->db->get();
		
		if ($query->num_rows() > 0)
		{
			$result = $query->row();
			
			// dapetin nama parent utama
			$name = '';
			$this->db->select('NAME');
			$this->db->from('KM_EVA_TAG');
			$this->db->where('ID = ', $tagset_id);
			$query3 = $this->db->get();
			
			$name = $query3->row()->NAME.'_';
			
			// get last ID
			$this->db->select('ID');
			$this->db->from('KM_EVA_TAG');
			$last_id = $this->db->get()->last_row();
		
			// dapetin data untuk previous tag
			$this->db->select('ID');
			$this->db->from('KM_EVA_TAG');
			$this->db->where('PARENT_TAG = ', $get_data['id']);
			$query4 = $this->db->get()->last_row();
			
			$previous_tag = $query4->ID;
			
			$data = array();
			$data['ID'] = $last_id->ID + 1;
			$data['ENV_ID'] = 666;
			$data['RELEASE_ID'] = 1;
			$data['TAGSET_ID'] = $tagset_id;
			$data['TAGSET_ENV_ID'] = 666;
			$data['TAGSET_RELEASE_ID'] = 1;
			$data['PARENT_TAG'] = $parent_tag;
			$data['PARENT_TAG_ENV_ID'] = 666;
			$data['PARENT_TAG_RELEASE_ID'] = 1;
			$data['PREVIOUS_TAG'] = $previous_tag;
			$data['PREVIOUS_TAG_ENV_ID'] = 666;
			$data['PREVIOUS_TAG_RELEASE_ID'] = 1;
			$data['NAME'] = $name;
			$data['IS_SYSTEM'] = 'N';
			$data['IS_DELETED'] = 'N';
			$data['IS_RETIRED'] = 'N';
			$data['DEPTH_VAL'] = $result->DEPTH_VAL + 1;
			$data['SYSTEM_CODE'] = $name;
			
			$this->db->set('CREATED_DATE', "TO_DATE('".date('Y-m-d H:i:s')."', 'yyyy-mm-dd HH24:MI:SS')", FALSE);
			$this->db->set('LAST_MODIFIED_DATE', "TO_DATE('".date('Y-m-d H:i:s')."', 'yyyy-mm-dd HH24:MI:SS')", FALSE);
			$query2 = $this->db->insert('KM_EVA_TAG', $data);
			
			if ($query2 == true)
			{
				// create display
				$param = array();
				$param['ID'] = $data['ID'];
				$param['ENV_ID'] = $data['ENV_ID'];
				$param['RELEASE_ID'] = $data['RELEASE_ID'];
				$param['LOCALE'] = 'en-GB';
				$param['DISPLAY_NAME'] = 'Tag Baru';
				$param['DISPLAY_NAME_TYPE_ID'] = 1;
				$param['DISPLAY_NAME_TYPE_ENV_ID'] = 666;
				$param['DISPLAY_NAME_TYPE_RELEASE_ID'] = 1;
				$query3 = $this->db->insert('KM_EVA_TAG_DISPLAY_NAME_LOC', $param);
				
				// get reporting label -> diambil dari parent utama sampai child terakhir
				$reporting = $param['DISPLAY_NAME'];
				
				for ($i = 1; $i <= $data['DEPTH_VAL']; $i++)
				{
					$this->db->select('KM_EVA_TAG.ID, NAME, PARENT_TAG, DEPTH_VAL, DISPLAY_NAME');
					$this->db->from('KM_EVA_TAG');
					$this->db->join('KM_EVA_TAG_DISPLAY_NAME_LOC', 'KM_EVA_TAG.ID = KM_EVA_TAG_DISPLAY_NAME_LOC.ID', 'LEFT');
					$this->db->where('KM_EVA_TAG.ID = ', $parent_tag);
					$query5 = $this->db->get();
					
					if ($query5->num_rows() > 0)
					{
						$parent_tag = $query5->row()->PARENT_TAG;
						$reporting = $query5->row()->DISPLAY_NAME.' => '.$reporting;
					}
				}
				
				$param2 = array();
				$param2['ID'] = $data['ID'];
				$param2['ENV_ID'] = $data['ENV_ID'];
				$param2['RELEASE_ID'] = $data['RELEASE_ID'];
				$param2['LOCALE'] = 'en-GB';
				$param2['DESCRIPTION'] = 'description for Tag Baru';
				$param2['REPORTING_LABEL'] = $reporting;
				$query6 = $this->db->insert('KM_EVA_TAG_LOC', $param2);
				
				echo json_encode($data);
			}
			else
			{
				$data = array();
				echo json_encode($data);
			}
		}
	}
	
	function tag_edit()
	{
		$get_data = $this->input->get('data');
		$get_data_new = json_decode($this->input->get('dataNew'));
		
		if ($get_data_new == TRUE)
		{
			$id = $get_data_new->ID;
		}
		else
		{
			$id = $get_data['id'];
		}
		
		// ambil data selected node
		$this->db->select('ID, TAGSET_ID');
		$this->db->from('KM_EVA_TAG');
		$this->db->where('ID = ', $id);
		$query = $this->db->get();
		
		if ($query->num_rows() > 0)
		{
			$result = $query->row();
			
			// ambil nama tagset ID
			$this->db->select('ID, NAME');
			$this->db->from('KM_EVA_TAG');
			$this->db->where('TAGSET_ID = ', $result->TAGSET_ID);
			$query3 = $this->db->get();
			$result2 = $query3->row();
			
			// rename nama selected node
			$name = str_replace(' ', '', strtolower($get_data['text']));
			
			$data = array();
			$data['NAME'] = $result2->NAME.'_'.$name;
			$data['SYSTEM_CODE'] = $result2->NAME.'_'.$name;
			
			$this->db->set('LAST_MODIFIED_DATE', "TO_DATE('".date('Y-m-d H:i:s')."', 'yyyy-mm-dd HH24:MI:SS')", FALSE);
			$this->db->where('ID', $id);
			$query2 = $this->db->update('KM_EVA_TAG', $data);
			
			// rename display name
			$param = array();
			$param['DISPLAY_NAME'] = $get_data['text'];
			
			$this->db->where('ID', $id);
			$query4 = $this->db->update('KM_EVA_TAG_DISPLAY_NAME_LOC', $param);
			
			// rename description & reporting label
			$reporting = '';
			$this->db->select('ID, REPORTING_LABEL');
			$this->db->from('KM_EVA_TAG_LOC');
			$this->db->where('ID = ', $id);
			$query5 = $this->db->get();
			
			$explode = explode('=>', $query5->row()->REPORTING_LABEL);
			array_pop($explode);
			$reporting = implode('=>', $explode).' => '.$get_data['text'];

			$param2 = array();
			$param2['DESCRIPTION'] = 'description for '.$get_data['text'];
			$param2['REPORTING_LABEL'] = $reporting;
			
			$this->db->where('ID', $id);
			$query4 = $this->db->update('KM_EVA_TAG_LOC', $param2);
			
			echo json_encode($query2);
		}
	}
	
	function tag_delete()
	{
		$get_data = $this->input->get('data');
		
		$query = $this->db->delete('KM_EVA_TAG', array('ID' => $get_data[0]['id']));
		$query2 = $this->db->delete('KM_EVA_TAG_LOC', array('ID' => $get_data[0]['id']));
		$query3 = $this->db->delete('KM_EVA_TAG_DISPLAY_NAME_LOC', array('ID' => $get_data[0]['id']));
		echo json_encode($query);
	}
	
	function ajax_feedback_regions()
	{
		$reg = new ManageTagsetModel;
		$regions = $reg->get_allfbcode(); 
		
		$data = array();
        $no = $_POST['start'];
        foreach ($regions as $feedback) {
            $no++;
            $row = array();
            $row[] = $feedback->DESCRIPTION;
            $row[] = $feedback->IS_DELETED;
			// $row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_fbcode('."'".$feedback->ID."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
                  // <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_fbcode('."'".$feedback->ID."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
			$row[] = '<a class="btn btn-sm btn-primary" href="'.base_url().'editing_tagset'.'" title="Edit" onclick="edit_fbcode('."'".$feedback->ID."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
                  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_fbcode('."'".$feedback->ID."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
  
            $data[] = $row;
        }
 
        $output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $reg->count_all(),
			"recordsFiltered" => $reg->count_filtered(),
			"data" => $data,
        );
        //output to json format
        echo json_encode($output);	
	}
	
	public function ajax_tag_edit($id){
		$fbcode = new ManageTagsetModel;
        $data   = $fbcode->get_tag_by_id($id);
        echo json_encode($data);
    }
	
	public function ajax_fbcode_add(){
		$fbcode = new FeedbackCodeModel;
        $this->_validate();
        $data = array(
			'NAME' => $this->input->post('name'),
			'DISPLAY_NAME' => $this->input->post('displayName'),
			'DESCRIPTION' => $this->input->post('desc'),
			'ADDITIONAL_ACTION_NEEDED' => $this->input->post('addAction'),
		);
        $insert = $fbcode->save($data);
        echo json_encode(array("status" => TRUE));
    }
 
    public function ajax_tag_update(){
		$tag = new ManageTagsetModel;
        $this->_validate();
        $data = array(
			// 'DISPLAY_NAME' => $this->input->post('displayName'),
			'DESCRIPTION' => $this->input->post('desc'),
			'IS_DELETED' => $this->input->post('addAction'),
		);
		
        // $tag->update(array('ID' => $this->input->post('id')), $data);
        $tag->update($this->input->post('id'), $this->input->post('desc'),$this->input->post('addAction'));
        echo json_encode(array("status" => TRUE));  
    }
 
    public function ajax_fbcode_delete($id){
		$fbcode = new FeedbackCodeModel;
        $fbcode->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }
 
 
    private function _validate(){
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;
 
        if($this->input->post('desc') == '')
        {
            $data['inputerror'][] = 'name';
            $data['error_string'][] = 'Name Tag is required';
            $data['status'] = FALSE;
        }
 
        if($this->input->post('addAction') == '')
        {
            $data['inputerror'][] = 'addAction';
            $data['error_string'][] = 'Is Deleted is required';
            $data['status'] = FALSE;
        }
 
        if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }
	
	
}
