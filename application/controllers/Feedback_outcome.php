<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Feedback_outcome extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
		$this->load->model('FeedbackOutcomeModel');	
	}

	public function index()
	{
		$this->load->view('view_feedback_outcome');
	}
	
	public function ajax_fbout_list(){
		$fbout       = new FeedbackOutcomeModel;
		$list = $fbout->get_allfbout();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $feedback) {
            $no++;
            $row = array();
            $row[] = $feedback->DISPLAY_NAME;
            $row[] = $feedback->DESCRIPTION;
			$row[] = '<a class="btn btn-sm btn-primary" href="javascript:void(0)" title="Edit" onclick="edit_fbout('."'".$feedback->ID."'".')"><i class="glyphicon glyphicon-pencil"></i> Edit</a>
                  <a class="btn btn-sm btn-danger" href="javascript:void(0)" title="Hapus" onclick="delete_fbout('."'".$feedback->ID."'".')"><i class="glyphicon glyphicon-trash"></i> Delete</a>';
  
            $data[] = $row;
        }
 
        $output = array(
			"draw" => $_POST['draw'],
			"recordsTotal" => $fbout->count_all(),
			"recordsFiltered" => $fbout->count_filtered(),
			"data" => $data,
        );
        //output to json format
        echo json_encode($output);
	}
	
	public function ajax_fbout_edit($id){
		$fbout = new FeedbackOutcomeModel;
        $data   = $fbout->get_fbout_by_id($id);
        echo json_encode($data);
    }
	
	public function ajax_fbout_add(){
		$fbout = new FeedbackOutcomeModel;
        $this->_validate();
        $data = array(
			'NAME' => $this->input->post('name'),
			'DISPLAY_NAME' => $this->input->post('displayName'),
			'DESCRIPTION' => $this->input->post('desc'),
		);
        $insert = $fbout->save($data);
        echo json_encode(array("status" => TRUE));
    }
 
    public function ajax_fbout_update(){
		$fbout = new FeedbackOutcomeModel;
        $this->_validate();
        $data = array(
			'NAME' => $this->input->post('name'),
			'DISPLAY_NAME' => $this->input->post('displayName'),
			'DESCRIPTION' => $this->input->post('desc'),
		);
        $fbout->update(array('ID' => $this->input->post('id')), $data);
        echo json_encode(array("status" => TRUE)); die;
    }
 
    public function ajax_fbout_delete($id){
		$fbout = new FeedbackOutcomeModel;
        $fbout->delete_by_id($id);
        echo json_encode(array("status" => TRUE));
    }
 
 
    private function _validate(){
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;
 
        if($this->input->post('name') == '')
        {
            $data['inputerror'][] = 'name';
            $data['error_string'][] = 'Name is required';
            $data['status'] = FALSE;
        }
 
        if($this->input->post('displayName') == '')
        {
            $data['inputerror'][] = 'displayName';
            $data['error_string'][] = 'Display name is required';
            $data['status'] = FALSE;
        }
 
        if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }
}
